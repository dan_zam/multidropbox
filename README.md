# MultiDropbox

Last Update: 20/09/2020  
Thanks to [https://stackoverflow.com/a/37383252](https://stackoverflow.com/a/37383252) and use it at your own risk.

# Make it work

* Install dropbox with wget ecc.. as described in the Dropbox website. (You can even install dropbox in other ways, but then you'll need to link to the corresponding executable.) 
* Create a folder for each one of your desired accounts, e.g., `dropbox-personal` and `dropbox-business`.
* Create a symlink for each folder `ln -s "$HOME/.Xauthority" /home/$USER/dropbox-folder1`
* Link the accounts with `HOME=/home/$USER/dropbox-folder1 && /home/$USER/.dropbox-dist/dropboxd`.
* If necessary, open `multidropbox.sh` file and edit the the path to Dropbox executable.
* Make `multidropbox.sh` executable.
* Run the following command:
```bash
multidropbox.sh account-folder-1 [account-folder-2 ...]
```
where folders `account-folder-*` are absolute paths.
