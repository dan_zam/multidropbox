#!/bin/bash

# Edit this line, if necessary.
STD_DROPBOX_EXEC=.dropbox-dist/dropbox-lnx.x86_64-105.4.651/dropbox

# Read option flags
FOREGROUND=false
FOLDERS=()
while [ ! $# -eq 0 ]
do
	case "$1" in
		--help | -h)
		    echo "Please, have a look at the README.md for some help."
            exit 
            ;;
		--foreground | -f)
			FOREGROUND=true
			;;
        *)
            FOLDERS+=($1)
	esac
	shift
done

# Run Dropbox
for FOLDER in "${FOLDERS[@]}"
do
    if $FOREGROUND;
    then HOME=$FOLDER $HOME/$STD_DROPBOX_EXEC start -i ;
    else HOME=$FOLDER $HOME/$STD_DROPBOX_EXEC start -i & 
    fi
done

